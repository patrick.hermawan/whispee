package common

import (
	"github.com/joho/godotenv"

	"github.com/kelseyhightower/envconfig"
)

type Env struct {
	Env                string `envconfig:"ENV"`
	LogLevel           string `envconfig:"LOG_LEVEL"` // panic, fatal, error, warning, info, debug, trace
	Port               int    `envconfig:"HTTP_PORT"`
	DbHost             string `envconfig:"DB_HOST"`
	DbUser             string `envconfig:"DB_USER"`
	DbPassword         string `envconfig:"DB_PASSWORD"`
	DbName             string `envconfig:"DB_NAME"`
	DbPort             string `envconfig:"DB_PORT"`
	AuthSecretKey      string `envconfig:"AUTH_SECRET_KEY"`
	HTTPRequestTimeout int    `envconfig:"HTTP_REQUEST_TIMEOUT" default:"2000"`
}

func GetEnv() Env {
	var env Env
	err := godotenv.Load(".env")
	if err != nil {
		panic(err)
	}
	err = envconfig.Process("", &env)
	if err != nil {
		panic(err)
	}
	return env
}
