package constant

import "time"

const (
	OneDay   = time.Hour * 24
	OneWeek  = OneDay * 7
	OneMonth = OneDay * 30
)
