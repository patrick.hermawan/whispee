package common

import (
	"crypto/sha512"
	"fmt"
)

func Encrypt(pass, salt string) string {
	h := sha512.New()
	h.Write([]byte(pass + salt))
	bs := h.Sum(nil)

	return fmt.Sprintf("%x", bs)
}
