package common

import (
	"errors"

	"github.com/gin-gonic/gin"
)

func GetTokenIdentifier(ctx *gin.Context, key string) (int, error) {
	value := ctx.Keys[key]
	if value == nil {
		return 0, errors.New("fail to get value")
	}
	parsedValue := int(value.(float64))
	return parsedValue, nil
}
