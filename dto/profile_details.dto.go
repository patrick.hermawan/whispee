package dto

import (
	"time"
)

type ProfileDetails struct {
	ID          int       `json:"id"`
	Displayname string    `json:"displayname"`
	PhoneNumber *string   `json:"phoneNumber"`
	BirthYear   *int16    `json:"birthYear"`
	Gender      *string   `json:"gender"`
	CreatedAt   time.Time `json:"createdAt"`
	UpdatedAt   time.Time `json:"updatedAt"`
}
