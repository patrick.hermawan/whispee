package dto

type ProfileCreateReq struct {
	DeviceID    string  `json:"deviceId"`
	Displayname *string `json:"displayname"`
	PhoneNumber *string `json:"phoneNumber"` // TODO validate
	Password    *string `json:"password"`
	BirthYear   *int16  `json:"birthYear"`
	Gender      *string `json:"gender"`
}
