package dto

import (
	"time"
)

type PostDetails struct {
	ID                 int           `json:"id"`
	ProfileID          int           `json:"profileID"`
	Likes              int           `json:"likes"`
	Image              string        `json:"image"`
	Content            string        `json:"content"`
	ProfileDisplayname string        `json:"profileDisplayname"`
	ProfileBirthyear   *int16        `json:"profileBirthyear"`
	ProfileGender      *string       `json:"profileGender"`
	Lat                float32       `json:"lat"`
	Lon                float32       `json:"lon"`
	ReplyTo            *PostDetails  `json:"replyTo"` // TODO limit
	Replies            []PostDetails `json:"replies"` // TODO limit
	CreatedAt          time.Time     `json:"createdAt"`
	UpdatedAt          time.Time     `json:"updatedAt"`
}
