package dto

type ChatRoomCreateReq struct {
	ProfileID int `json:"profileID"` // only for socket.io. HTTP ignores this (uses JWT)
	PostID    int `json:"postID"`    // TODO groups
}
