package dto

type LoginProfileReq struct {
	ID          *int    `json:"id"`
	PhoneNumber *string `json:"phoneNumber"`
	Password    string  `json:"password"`
}
