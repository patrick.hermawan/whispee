package dto

import (
	"time"
)

type ChatRoomDetails struct {
	ID             int              `json:"id"`
	Post           PostDetails      `json:"post"`
	MoreOlderChats bool             `json:"moreOlderChats"`
	Chats          []ChatDetails    `json:"chats"`
	Members        []ProfileDetails `json:"members"`
	CreatedAt      time.Time        `json:"createdAt"`
	UpdatedAt      time.Time        `json:"updatedAt"`
}

type ChatDetails struct {
	ID           int       `json:"id"`
	Text         string    `json:"text"` // TODO attachments
	FrontendUUID string    `json:"frontendUUID"`
	CreatedAt    time.Time `json:"createdAt"`
	ReplyTo      int       `json:"replyTo"`
	ChatRoomID   int       `json:"chatRoomID"`
}
