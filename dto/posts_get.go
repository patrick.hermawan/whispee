package dto

type PostsGetReq struct {
	LastID           *int    `form:"lastID"`
	Limit            *int    `form:"limit"`
	Lat              float32 `form:"lat"`
	Lon              float32 `form:"lon"`
	Distance         float32 `form:"distance"`
	IsMale           *bool   `form:"isMale"`
	MinAge           *int16  `form:"minAge"`
	MaxAge           *int16  `form:"maxAge"`
	SortByPopularity bool    `form:"sortByPopularity"`
	ProfileID        *int    `form:"userID"`
}
