package dto

type ChatSendReq struct {
	ProfileID    int    `json:"profileID"`
	ChatRoomID   int    `json:"chatRoomID"`
	Text         string `json:"text"` // TODO implement attachments
	FrontendUUID string `json:"frontendUUID"`
	ReplyTo      *int   `json:"replyTo"`
}
