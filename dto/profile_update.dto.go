package dto

type ProfileUpdateReq struct {
	Displayname *string `json:"displayname"`
	PhoneNumber *string `json:"phoneNumber"`
	Password    *string `json:"password"`
	BirthYear   *int16  `json:"birthYear"`
	Gender      *string `json:"gender"`
}
