package dto

type PostCreateReq struct {
	Image   string  `json:"image" binding:"required"`
	Content string  `json:"content" binding:"required"` // TODO validate
	Lat     float32 `json:"lat"`
	Lon     float32 `json:"lon"`
	ReplyTo *int    `json:"replyTo"`
}


