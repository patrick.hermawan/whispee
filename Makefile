run:
	go run main.go

test:
	go test ./...

test_coverage:
	echo "test coverage"

generate-ent:
	go generate ./ent
