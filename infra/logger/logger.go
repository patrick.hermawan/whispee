package logger

import (
	"fmt"
	"os"
	"strings"

	"github.com/sirupsen/logrus"
)

type Logger struct {
	level   string
	options map[string]string
}

func New(level string) *Logger {
	loglevel, ok := getLogLevel(level)
	if !ok {
		panic(fmt.Sprintf("invalid logrus level %v", level))
	}
	logrus.SetLevel(loglevel)
	logrus.SetFormatter(&logrus.JSONFormatter{})
	logrus.SetOutput(os.Stdout)

	return &Logger{
		level: level,
	}
}

func (l *Logger) Debug(message string, fields map[string]interface{}) {
	logrus.WithFields(fields).Debug(message)
}

func (l *Logger) Warn(message string, fields map[string]interface{}) {
	logrus.WithFields(fields).Warn(message)
}

func (l *Logger) Info(message string, fields map[string]interface{}) {
	logrus.WithFields(fields).Info(message)
}

func (l *Logger) Error(message string, fields map[string]interface{}) {
	logrus.WithFields(fields).Error(message)
}

func (l *Logger) Fatal(message string, fields map[string]interface{}) {
	logrus.WithFields(fields).Fatal(message)
}

func getLogLevel(level string) (logrus.Level, bool) {
	lvl := strings.ToLower(level)
	levels := map[string]logrus.Level{
		"debug": logrus.DebugLevel,
		"warn":  logrus.WarnLevel,
		"info":  logrus.InfoLevel,
		"error": logrus.ErrorLevel,
		"fatal": logrus.FatalLevel,
	}

	loglevel, ok := levels[lvl]

	if !ok {
		return 0, false
	}

	return loglevel, true
}
