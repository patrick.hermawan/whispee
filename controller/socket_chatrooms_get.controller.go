package controller

import (
	"context"
	"encoding/json"

	socketio "github.com/googollee/go-socket.io"
)

func (c *Controller) SocketChatRoomsGet(s socketio.Conn, profileID int) (*string, *string) {
	ctx := context.Background()
	s.SetContext(profileID)

	serviceResp, err := c.Service.ChatRoomsGet(ctx, profileID)
	if err != nil {
		errStr := error.Error(err)
		return nil, &errStr
	}
	c.Logger.Debug("[Controller][SocketChatRoomsGet] Called Service. Received: ",
		map[string]interface{}{"resp": serviceResp})

	jsonResp, err := json.Marshal(serviceResp)
	if err != nil {
		errStr := error.Error(err)
		return nil, &errStr
	}
	resp := string(jsonResp)
	return &resp, nil
}
