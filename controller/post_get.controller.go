package controller

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

func (c *Controller) PostGet(ctx *gin.Context) {
	postID := ctx.Param("id")
	postIDint, err := strconv.Atoi(postID)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, err.Error())
		return
	}

	resp, err := c.Service.PostGet(ctx, postIDint)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, err.Error())
		return
	}

	c.Logger.Debug("[Controller][PostGet] Called Service. Received: ",
		map[string]interface{}{"resp": resp})

	ctx.JSON(http.StatusOK, resp)
}
