package controller

import (
	"context"
	"encoding/json"
	"gochassis/dto"
	"strconv"

	socketio "github.com/googollee/go-socket.io"
)

func (c *Controller) SocketChatRoomCreate(s socketio.Conn, req dto.ChatRoomCreateReq) (*string, *string) {
	ctx := context.Background()

	s.SetContext(req)
	serviceResp, err := c.Service.ChatRoomCreate(ctx, req.ProfileID, req)
	if err != nil {
		errStr := error.Error(err)
		return nil, &errStr
	}
	c.Logger.Debug("[Controller][SocketChatRoomJoin] Called Service. Received: ",
		map[string]interface{}{"resp": serviceResp})

	s.Join(strconv.Itoa(serviceResp.ID))
	c.Logger.Debug("[Controller][SocketChatRoomJoin] Joined Room: ",
		map[string]interface{}{"roomID": strconv.Itoa(serviceResp.ID)})

	jsonResp, err := json.Marshal(serviceResp)
	if err != nil {
		errStr := error.Error(err)
		return nil, &errStr
	}
	resp := string(jsonResp)
	return &resp, nil
}
