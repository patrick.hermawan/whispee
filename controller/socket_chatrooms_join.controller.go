package controller

import (
	"context"
	"encoding/json"
	"strconv"

	socketio "github.com/googollee/go-socket.io"
)

func (c *Controller) SocketChatRoomJoin(s socketio.Conn, roomIDs []int) (*string, *string) {
	ctx := context.Background()

	s.SetContext(roomIDs)
	serviceResp, err := c.Service.ChatRoomsJoin(ctx, roomIDs)
	if err != nil {
		errStr := error.Error(err)
		return nil, &errStr
	}
	c.Logger.Debug("[Controller][SocketChatRoomJoin] Called Service. Received: ",
		map[string]interface{}{"resp": serviceResp})

	for _, chatRoom := range serviceResp {
		s.Join(strconv.Itoa(chatRoom.ID))
		c.Logger.Debug("[Controller][SocketChatRoomJoin] Joined Room: ",
			map[string]interface{}{"roomID": strconv.Itoa(chatRoom.ID)})
	}

	jsonResp, err := json.Marshal(serviceResp)
	if err != nil {
		errStr := error.Error(err)
		return nil, &errStr
	}
	resp := string(jsonResp)
	return &resp, nil
}
