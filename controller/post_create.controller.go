package controller

import (
	"gochassis/common"
	"gochassis/common/constant"
	"gochassis/dto"
	"net/http"

	"github.com/gin-gonic/gin"
)

func (c *Controller) PostCreate(ctx *gin.Context) {
	profileID, err := common.GetTokenIdentifier(ctx, constant.ProfileIdentifier)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "cannot get key " + constant.ProfileIdentifier})
		return
	}
	c.Logger.Debug("[Controller][PostCreate] Received: ",
		map[string]interface{}{"profileID": profileID},
	)

	var req dto.PostCreateReq
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	c.Logger.Debug("[Controller][PostCreate] Received: ",
		map[string]interface{}{"req": req},
	)

	resp, err := c.Service.PostCreate(ctx, profileID, req)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, err.Error())
		return
	}

	c.Logger.Debug("[Controller][PostCreate] Called Service. Received: ",
		map[string]interface{}{"resp": resp})

	ctx.JSON(http.StatusOK, resp)
}
