package controller

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func (c *Controller) PingHandler(ctx *gin.Context) {
	c.Logger.Info("creating ping handler", nil)

	ctx.String(http.StatusOK, "pong")
}
