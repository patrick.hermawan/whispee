package controller

import (
	"context"
	"encoding/json"

	socketio "github.com/googollee/go-socket.io"
)

func (c *Controller) SocketChatGet(s socketio.Conn, req map[int]int) (*string, *string) {
	ctx := context.Background()

	s.SetContext(req)
	serviceResp, err := c.Service.ChatGet(ctx, req)
	if err != nil {
		errStr := error.Error(err)
		return nil, &errStr
	}
	c.Logger.Debug("[Controller][SocketChatGet] Called Service. Received: ",
		map[string]interface{}{"resp": serviceResp})

	jsonResp, err := json.Marshal(serviceResp)
	if err != nil {
		errStr := error.Error(err)
		return nil, &errStr
	}
	resp := string(jsonResp)
	return &resp, nil
}
