package controller

import (
	"gochassis/dto"
	"net/http"

	"github.com/gin-gonic/gin"
)

func (c *Controller) PostsGet(ctx *gin.Context) {
	req := dto.PostsGetReq{}
	if err := ctx.ShouldBindQuery(&req); err != nil {
		ctx.JSON(http.StatusInternalServerError, err.Error())
	}

	resp, err := c.Service.PostsGet(ctx, nil, req)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, err.Error())
		return
	}

	c.Logger.Debug("[Controller][PostsGet] Called Service. Received: ",
		map[string]interface{}{"resp": resp})

	ctx.JSON(http.StatusOK, resp)
}
