package controller

import (
	"gochassis/common/constant"
	"gochassis/dto"

	jwt "github.com/appleboy/gin-jwt/v2"
	"github.com/gin-gonic/gin"
)

func (c *Controller) GetProfileMiddleware() (*jwt.GinJWTMiddleware, error) {
	return jwt.New(&jwt.GinJWTMiddleware{
		Realm:       "profile auth",
		Key:         []byte(c.Service.Env.AuthSecretKey),
		Timeout:     constant.OneWeek,
		MaxRefresh:  constant.OneMonth,
		IdentityKey: constant.ProfileIdentifier,
		PayloadFunc: func(data interface{}) jwt.MapClaims {
			return jwt.MapClaims{
				constant.ProfileIdentifier: data,
			}
		},
		IdentityHandler: func(c *gin.Context) interface{} {
			claims := jwt.ExtractClaims(c)
			return claims[constant.ProfileIdentifier]
		},
		Authenticator: func(ctx *gin.Context) (interface{}, error) {
			var req dto.LoginProfileReq
			if err := ctx.ShouldBindJSON(&req); err != nil {
				return nil, err
			}

			profileID, err := c.Service.LoginProfile(ctx, req)
			if err != nil {
				return nil, jwt.ErrFailedAuthentication
			}
			return profileID, nil
		},
	})
}
