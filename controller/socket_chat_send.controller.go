package controller

import (
	"context"
	"encoding/json"
	"gochassis/dto"
	"strconv"

	socketio "github.com/googollee/go-socket.io"
)

func (c *Controller) SocketChatSend(s socketio.Conn, req dto.ChatSendReq) (*string, *string) {
	ctx := context.Background()

	s.SetContext(req)
	serviceResp, err := c.Service.ChatSend(ctx, req)
	if err != nil {
		errStr := error.Error(err)
		return nil, &errStr
	}
	c.Logger.Debug("[Controller][SocketChatSend] Called Service. Received: ",
		map[string]interface{}{"resp": serviceResp})

	jsonResp, err := json.Marshal(serviceResp)
	if err != nil {
		errStr := error.Error(err)
		return nil, &errStr
	}
	resp := string(jsonResp)

	c.SocketServer.BroadcastToRoom(
		"/chat",
		strconv.Itoa(req.ChatRoomID),
		"msg",
		resp,
	)

	return &resp, nil
}
