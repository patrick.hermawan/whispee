package controller

import (
	"gochassis/infra/logger"
	"gochassis/service"

	socketio "github.com/googollee/go-socket.io"
)

type Controller struct {
	Service      service.Service
	SocketServer *socketio.Server
	Logger       logger.Logger
}
