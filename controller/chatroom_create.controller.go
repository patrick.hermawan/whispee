package controller

import (
	"gochassis/common"
	"gochassis/common/constant"
	"gochassis/dto"
	"net/http"

	"github.com/gin-gonic/gin"
)

func (c *Controller) ChatRoomCreate(ctx *gin.Context) {
	profileID, err := common.GetTokenIdentifier(ctx, constant.ProfileIdentifier)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "cannot get key " + constant.ProfileIdentifier})
		return
	}
	c.Logger.Debug("[Controller][ChatRoomCreate] Profile ID: ",
		map[string]interface{}{"ID": profileID},
	)

	var req dto.ChatRoomCreateReq
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	c.Logger.Debug("[Controller][ChatRoomCreate] Received: ",
		map[string]interface{}{"req": req},
	)

	resp, err := c.Service.ChatRoomCreate(ctx, profileID, req)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, err)
		return
	}
	c.Logger.Debug("[Controller][ChatRoomCreate] Called Service. Received: ",
		map[string]interface{}{"resp": resp})

	ctx.JSON(http.StatusOK, resp)
}
