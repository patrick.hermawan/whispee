package controller

import (
	"fmt"

	socketio "github.com/googollee/go-socket.io"
)

func (c *Controller) SocketConnect(s socketio.Conn) error {
	s.SetContext("")
	fmt.Println("connected:", s.ID())
	return nil // TODO complete
}
