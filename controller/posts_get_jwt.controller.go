package controller

import (
	"gochassis/common"
	"gochassis/common/constant"
	"gochassis/dto"
	"net/http"

	"github.com/gin-gonic/gin"
)

func (c *Controller) PostsGetJWT(ctx *gin.Context) {
	profileID, err := common.GetTokenIdentifier(ctx, constant.ProfileIdentifier)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "cannot get key " + constant.ProfileIdentifier})
		return
	}
	c.Logger.Debug("[Controller][PostsGetJWT] Profile ID: ",
		map[string]interface{}{"ID": profileID},
	)

	req := dto.PostsGetReq{}
	if err := ctx.ShouldBindQuery(&req); err != nil {
		ctx.JSON(http.StatusInternalServerError, err.Error())
	}
	c.Logger.Debug("[Controller][PostsGetJWT] Request: ",
		map[string]interface{}{"req": req},
	)

	resp, err := c.Service.PostsGet(ctx, &profileID, req)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, err.Error())
		return
	}
	c.Logger.Debug("[Controller][PostsGetJWT] Called Service. Received: ",
		map[string]interface{}{"resp": resp})

	ctx.JSON(http.StatusOK, resp)
}
