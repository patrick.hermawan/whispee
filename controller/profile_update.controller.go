package controller

import (
	"gochassis/common"
	"gochassis/common/constant"
	"gochassis/dto"
	"net/http"

	"github.com/gin-gonic/gin"
)

func (c *Controller) ProfileUpdate(ctx *gin.Context) {
	profileID, err := common.GetTokenIdentifier(ctx, constant.ProfileIdentifier)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "cannot get key " + constant.ProfileIdentifier})
		return
	}
	c.Logger.Debug("[Controller][ProfileUpdate] Profile ID: ",
		map[string]interface{}{"ID": profileID},
	)

	var req dto.ProfileUpdateReq
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	c.Logger.Debug("[Controller][ProfileUpdate] Received: ",
		map[string]interface{}{"req": req},
	)

	resp, err := c.Service.ProfileUpdate(ctx, profileID, req)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, err)
		return
	}

	c.Logger.Debug("[Controller][ProfileUpdate] Called Service. Received: ",
		map[string]interface{}{"resp": resp})

	ctx.JSON(http.StatusOK, resp)
}
