package controller

import (
	"gochassis/dto"
	"net/http"

	"github.com/gin-gonic/gin"
)

func (c *Controller) ProfileCreate(ctx *gin.Context) {
	var req dto.ProfileCreateReq
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	c.Logger.Debug("[Controller][ProfileCreate] Received: ",
		map[string]interface{}{"req": req},
	)

	resp, err := c.Service.ProfileCreate(ctx, req)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, err.Error())
		return
	}

	c.Logger.Debug("[Controller][ProfileCreate] Called Service. Received: ",
		map[string]interface{}{"resp": resp})

	ctx.JSON(http.StatusOK, resp)
}
