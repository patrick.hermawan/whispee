package service

import (
	"gochassis/dto"
	"gochassis/ent"
)

func (s *Service) ProfileDetailsFromEntity(profile ent.Profile) dto.ProfileDetails {
	return dto.ProfileDetails{
		ID:          profile.ID,
		Displayname: profile.Displayname,
		PhoneNumber: &profile.PhoneNumber,
		BirthYear:   &profile.BirthYear,
		Gender:      &profile.Gender,
		CreatedAt:   profile.CreatedAt,
		UpdatedAt:   profile.UpdatedAt,
	}
}
