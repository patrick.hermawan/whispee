package service

import (
	"context"
	"errors"
	"gochassis/common"
	"gochassis/dto"
	"gochassis/ent/profile"

	"github.com/google/uuid"
)

func (s *Service) ProfileCreate(ctx context.Context, req dto.ProfileCreateReq) (*dto.ProfileDetails, error) {
	if req.PhoneNumber != nil {
		if 0 < s.Repo.Profile.Query().Where(profile.PhoneNumber(*req.PhoneNumber)).CountX(ctx) { // TODO dont countx
			return nil, errors.New("existing profile found")
		}
	}

	newProfile := s.Repo.Profile.Create()
	newProfile = newProfile.SetDeviceID(req.DeviceID)

	if req.Displayname != nil {
		newProfile.SetDisplayname(*req.Displayname)
	} else {
		newProfile.SetDisplayname(uuid.New().String()) // TODO decide default displayname
	}
	if req.PhoneNumber != nil {
		newProfile.SetPhoneNumber(*req.PhoneNumber)
	}
	if req.Gender != nil {
		newProfile.SetGender(*req.Gender)
	}
	if req.BirthYear != nil {
		newProfile.SetBirthYear(*req.BirthYear)
	}
	if req.Password != nil {
		encryptedPassword := common.Encrypt(*req.Password, s.Env.AuthSecretKey)
		newProfile.SetPassword(encryptedPassword)
	}
	newProfileFromRepo, err := newProfile.Save(ctx)
	if err != nil {
		return nil, err
	}
	resp := s.ProfileDetailsFromEntity(*newProfileFromRepo)
	return &resp, nil
}
