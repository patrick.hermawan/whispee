package service

import (
	"context"
	"gochassis/dto"
	"gochassis/ent"
	"gochassis/ent/chat"
)

func (s *Service) ChatSend(ctx context.Context, req dto.ChatSendReq) (*dto.ChatDetails, error) {
	theChat, err := s.Repo.Chat.Query().
		Where(chat.FrontendUUID(req.FrontendUUID)).
		Only(ctx)
	if ent.IsNotFound(err) {
		newChat := s.Repo.Chat.Create()
		newChat.SetProfileID(req.ProfileID)
		newChat.SetChatRoomID(req.ChatRoomID)
		newChat.SetText(req.Text)
		newChat.SetFrontendUUID(req.FrontendUUID)
		newChat.SetNillableReplyTo(req.ReplyTo)

		theChat, err = newChat.Save(ctx)
	}
	if err != nil {
		return nil, err
	}
	resp := s.ChatDetailsFromEntity(ctx, *theChat)
	return &resp, nil
}
