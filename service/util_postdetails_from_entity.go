package service

import (
	"context"
	"gochassis/dto"
	"gochassis/ent"
)

func (s *Service) PostDetailsFromEntity(ctx context.Context, post *ent.Post, replyTo *ent.Post, replies []*ent.Post) (*dto.PostDetails, error) {
	if post == nil {
		return nil, nil
	}

	replyToPostDetails, err := s.PostDetailsFromEntity(ctx, replyTo, nil, nil)
	if err != nil {
		return nil, err
	}
	var repliesPostDetails []dto.PostDetails
	if replies != nil {
		for _, reply := range replies {
			replyPostDetails, err := s.PostDetailsFromEntity(ctx, reply, nil, nil)
			if err != nil {
				return nil, err
			}
			repliesPostDetails = append(repliesPostDetails, *replyPostDetails)
		}
	}

	profileID, err := post.QueryPostedBy().OnlyID(ctx)
	if err != nil {
		return nil, err
	}
	likesCount, err := post.QueryLikedBy().Count(ctx)
	if err != nil {
		return nil, err
	}
	return &dto.PostDetails{
		ID:                 post.ID,
		Image:              post.Image,
		Content:            post.Content,
		ProfileID:          profileID,
		ProfileDisplayname: post.ProfileDisplayname,
		ProfileBirthyear:   &post.ProfileBirthYear,
		ProfileGender:      &post.ProfileGender,
		Lat:                post.Coordinates[0],
		Lon:                post.Coordinates[1],
		Likes:              likesCount,
		ReplyTo:            replyToPostDetails,
		Replies:            repliesPostDetails,
	}, nil
}
