package service

import (
	"context"
	"gochassis/dto"
	"gochassis/ent"
	"gochassis/ent/chatroom"
	"gochassis/ent/post"
	"gochassis/ent/profile"
)

func (s *Service) ChatRoomCreate(ctx context.Context, profileID int, req dto.ChatRoomCreateReq) (*dto.ChatRoomDetails, error) {
	thePost, err := s.Repo.Post.Query().Where(post.ID(req.PostID)).Only(ctx)
	if err != nil {
		return nil, err
	}
	posterProfile, err := thePost.QueryPostedBy().Only(ctx)
	if err != nil {
		return nil, err
	}

	theChatRoom, err := s.Repo.ChatRoom.Query().
		Where(
			chatroom.HasPostWith(post.ID(req.PostID)),
			chatroom.HasProfilesWith(profile.ID(profileID), profile.ID(posterProfile.ID)),
		).Only(ctx)

	if ent.IsNotFound(err) {
		existingProfile, err := s.Repo.Profile.Query().Where(profile.ID(profileID)).Only(ctx)
		if err != nil {
			return nil, err
		}

		newChatRoom := s.Repo.ChatRoom.Create()
		newChatRoom = newChatRoom.SetPost(thePost)
		newChatRoom = newChatRoom.AddProfiles(existingProfile, posterProfile)

		theChatRoom, err = newChatRoom.Save(ctx)
	}
	if err != nil {
		return nil, err
	}

	resp, err := s.ChatRoomDetailsFromEntity(ctx, *theChatRoom, nil)
	if err != nil {
		return nil, err
	}
	return resp, nil
}
