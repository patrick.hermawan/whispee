package service

import (
	"context"
	"gochassis/ent/chat"
	"gochassis/ent/chatroom"
	"gochassis/ent/profile"

	"entgo.io/ent/dialect/sql"
)

func (s *Service) ChatRoomsGet(ctx context.Context, profileID int) (map[int]int, error) {
	theChatRooms, err := s.Repo.ChatRoom.Query().
		Where(chatroom.HasProfilesWith(profile.ID(profileID))).
		All(ctx)
	if err != nil {
		return nil, err
	}

	resp := map[int]int{}
	for _, theChatRoom := range theChatRooms {
		chatRoomID := theChatRoom.ID
		lastChatID, err := theChatRoom.QueryChats().
			Order(chat.ByID(sql.OrderDesc())).
			FirstID(ctx)
		if err != nil {
			return nil, err
		}
		resp[chatRoomID] = lastChatID
	}

	return resp, nil
}
