package service

import (
	"context"
	"gochassis/dto"
	"gochassis/ent/profile"
	"gochassis/ent/schema"
)

func (s *Service) PostCreate(ctx context.Context, profileID int, req dto.PostCreateReq) (*dto.PostDetails, error) {
	posterProfile, err := s.Repo.Profile.Query().Where(profile.ID(profileID)).Only(ctx)

	newPost := s.Repo.Post.Create()
	newPost.SetProfileDisplayname(posterProfile.Displayname)
	newPost.SetNillableProfileBirthYear(&posterProfile.BirthYear)
	newPost.SetNillableProfileGender(&posterProfile.Gender)

	newPost.SetImage(req.Image) // TODO is it link? UUID?
	newPost.SetContent(req.Content)
	newPost.SetCoordinates(&schema.Point{req.Lat, req.Lon})
	newPost.SetNillableReplyTo(req.ReplyTo)

	newPostFromRepo, err := newPost.Save(ctx)
	if err != nil {
		return nil, err
	}
	replyTo, err := newPostFromRepo.QueryReplyToEdge().Only(ctx)
	if err != nil {
		return nil, err
	}
	resp, err := s.PostDetailsFromEntity(ctx, newPostFromRepo, replyTo, nil)
	if err != nil {
		return nil, err
	}
	return resp, nil
}
