package service

import (
	"context"
	"gochassis/common"
	"gochassis/dto"
	"gochassis/ent/profile"
)

func (s *Service) ProfileUpdate(ctx context.Context, profileID int, req dto.ProfileUpdateReq) (*dto.ProfileDetails, error) {
	existingProfileBeforeUpdate, err := s.Repo.Profile.Query().Where(profile.ID(profileID)).Only(ctx)
	existingProfile := existingProfileBeforeUpdate.Update()

	if req.Displayname != nil {
		existingProfile.SetDisplayname(*req.Displayname)
	}
	if req.PhoneNumber != nil {
		existingProfile.SetPhoneNumber(*req.PhoneNumber)
	}
	if req.Gender != nil {
		existingProfile.SetGender(*req.Gender)
	}
	if req.BirthYear != nil {
		existingProfile.SetBirthYear(*req.BirthYear)
	}
	if req.Password != nil {
		encryptedPassword := common.Encrypt(*req.Password, s.Env.AuthSecretKey)
		existingProfile.SetPassword(encryptedPassword)
	}
	newProfileFromRepo, err := existingProfile.Save(ctx)
	if err != nil {
		return nil, err
	}

	resp := s.ProfileDetailsFromEntity(*newProfileFromRepo)
	return &resp, nil
}
