package service

import (
	"context"
	"errors"
	"gochassis/common"
	"gochassis/dto"
	"gochassis/ent"
	"gochassis/ent/profile"
)

func (s *Service) LoginProfile(ctx context.Context, req dto.LoginProfileReq) (*int, error) {
	theProfile, err := s.findByIdOrPhone(ctx, req)
	if err != nil {
		return nil, err
	}

	encryptedPassword := common.Encrypt(req.Password, s.Env.AuthSecretKey)

	if encryptedPassword != theProfile.Password {
		return nil, errors.New("salah password goblok") // TODO goblok
	}
	return &theProfile.ID, nil
}

func (s *Service) findByIdOrPhone(ctx context.Context, req dto.LoginProfileReq) (*ent.Profile, error) {
	if req.ID != nil {
		return s.Repo.Profile.Query().Where(profile.ID(*req.ID)).Only(ctx)
	} else {
		return s.Repo.Profile.Query().Where(profile.PhoneNumber(*req.PhoneNumber)).Only(ctx)
	}
}
