package service

import (
	"context"
	"gochassis/dto"
	"gochassis/ent/chat"
	"gochassis/ent/chatroom"
)

func (s *Service) ChatGet(ctx context.Context, req map[int]int) (map[int][]dto.ChatDetails, error) {
	chatRoomIDs := make([]int, len(req))

	theChats, err := s.Repo.Chat.Query().
		Where(chat.HasChatRoomWith(chatroom.IDIn(chatRoomIDs...))).
		All(ctx)
	if err != nil {
		return nil, err
	}

	resp := map[int][]dto.ChatDetails{}
	for _, theChat := range theChats {
		chatRoomID := theChat.QueryChatRoom().OnlyIDX(ctx)
		resp[chatRoomID] = append(resp[chatRoomID], s.ChatDetailsFromEntity(ctx, *theChat))
	}
	return resp, nil
}
