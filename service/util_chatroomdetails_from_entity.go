package service

import (
	"context"
	"gochassis/dto"
	"gochassis/ent"
	"gochassis/ent/chat"
	"math"
)

func (s *Service) ChatRoomDetailsFromEntity(ctx context.Context, chatRoom ent.ChatRoom, chats []*ent.Chat) (*dto.ChatRoomDetails, error) {
	post, err := chatRoom.QueryPost().Only(ctx)
	if err != nil {
		return nil, err
	}
	postDetails, err := s.PostDetailsFromEntity(ctx, post, nil, nil)
	chatDetails := []dto.ChatDetails{}
	for _, chat := range chats {
		if chat != nil {
			chatDetails = append(chatDetails, s.ChatDetailsFromEntity(ctx, *chat))
		}
	}

	profiles, err := chatRoom.QueryProfiles().All(ctx)
	if err != nil {
		return nil, err
	}
	profileDetails := []dto.ProfileDetails{}
	for _, profile := range profiles {
		profileDetails = append(profileDetails, s.ProfileDetailsFromEntity(*profile))
	}

	moreOlderChats, err := s.areThereChatsBefore(ctx, chatRoom, chats)
	if err != nil {
		return nil, err
	}

	return &dto.ChatRoomDetails{
		ID:             chatRoom.ID,
		Post:           *postDetails,
		Chats:          chatDetails,
		MoreOlderChats: moreOlderChats,
		Members:        profileDetails,
		CreatedAt:      chatRoom.CreatedAt,
		UpdatedAt:      chatRoom.UpdatedAt,
	}, nil
}

func (s *Service) areThereChatsBefore(ctx context.Context, chatRoom ent.ChatRoom, chats []*ent.Chat) (bool, error) {
	chatMinID := math.MaxInt
	for _, i := range chats {
		if i != nil && chatMinID > i.ID {
			chatMinID = i.ID
		}
	}
	chatsBeforeCount, err := chatRoom.QueryChats().
		Where(chat.IDLT(chatMinID)).
		Count(ctx)
	if err != nil {
		return false, err
	}
	return chatsBeforeCount > 0, nil
}

func (s *Service) ChatDetailsFromEntity(ctx context.Context, chat ent.Chat) dto.ChatDetails {
	return dto.ChatDetails{
		ID:           chat.ID,
		Text:         chat.Text,
		FrontendUUID: chat.FrontendUUID,
		CreatedAt:    chat.CreatedAt,
		ReplyTo:      chat.ReplyTo,
		ChatRoomID:   chat.QueryChatRoom().OnlyIDX(ctx),
	}
}
