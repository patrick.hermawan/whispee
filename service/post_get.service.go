package service

import (
	"context"
	"gochassis/dto"
	"gochassis/ent"
	"gochassis/ent/post"
)

func (s *Service) PostGet(ctx context.Context, postID int) (*dto.PostDetails, error) {
	thePost, err := s.Repo.Post.Query().Where(post.ID(postID)).Only(ctx)

	replyTo, err := thePost.QueryReplyToEdge().Only(ctx)
	if err != nil && !ent.IsNotFound(err) {
		return nil, err
	}
	replies, err := thePost.QueryReplies().All(ctx)
	if err != nil {
		return nil, err
	}

	resp, err := s.PostDetailsFromEntity(ctx, thePost, replyTo, replies)
	return resp, nil
}
