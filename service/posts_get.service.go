package service

import (
	"context"
	"gochassis/common/constant"
	"gochassis/dto"
	"gochassis/ent"
	"gochassis/ent/post"
	"gochassis/ent/profile"
	"time"

	"entgo.io/ent/dialect/sql"
)

func (s *Service) PostsGet(ctx context.Context, profileID *int, req dto.PostsGetReq) ([]dto.PostDetails, error) {
	thePostsQuery := s.Repo.Post.Query()
	thePostsQuery = thePostsQuery.Where(func(s *sql.Selector) {
		s.Where(sql.ExprP("ST_DISTANCE_SPHERE(coordinates, POINT(?, ?)) < ?", req.Lon, req.Lat, req.Distance))
	}) // TODO implement without custom expression
	thePostsQuery = thePostsQuery.Limit(100)
	//TODO Limit, IsMale, MinAge, MaxAge
	if profileID != nil {
		thePostsQuery = thePostsQuery.Where(post.HasPostedByWith(profile.ID(*profileID)))
	}
	if req.SortByPopularity {
		yesterday := time.Now().Add(-constant.OneDay)
		thePostsQuery = thePostsQuery.Where(post.CreatedAtGTE(yesterday))
		thePostsQuery = thePostsQuery.Order(post.ByLikedByCount(sql.OrderDesc()))
	} else {
		thePostsQuery = thePostsQuery.Order(ent.Desc(post.FieldID))
		if req.LastID != nil {
			thePostsQuery = thePostsQuery.Where(post.IDLT(*req.LastID))
		}
	}
	thePosts, err := thePostsQuery.All(ctx)

	if err != nil {
		return nil, err
	}

	resp := []dto.PostDetails{}
	for _, thePost := range thePosts {
		postDetails, err := s.PostDetailsFromEntity(ctx, thePost, nil, nil)
		if err != nil {
			return nil, err
		}
		resp = append(resp, *postDetails)
	}
	return resp, nil
}
