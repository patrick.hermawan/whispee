package service

import (
	"context"
	"gochassis/dto"
	"gochassis/ent/chat"
	"gochassis/ent/chatroom"

	"entgo.io/ent/dialect/sql"
)

func (s *Service) ChatRoomsJoin(ctx context.Context, chatRoomIDs []int) (map[int]dto.ChatRoomDetails, error) {
	theChatRooms, err := s.Repo.ChatRoom.Query().
		Where(chatroom.IDIn(chatRoomIDs...)).
		All(ctx)
	if err != nil {
		return nil, err
	}

	resp := map[int]dto.ChatRoomDetails{}
	for _, theChatRoom := range theChatRooms {
		chats, err := theChatRoom.QueryChats().
			Limit(100).
			Order(chat.ByID(sql.OrderDesc())).
			All(ctx)
		if err != nil {
			return nil, err
		}

		chatroomDetails, err := s.ChatRoomDetailsFromEntity(ctx, *theChatRoom, chats)
		if err != nil {
			return nil, err
		}
		resp[theChatRoom.ID] = *chatroomDetails
	}
	return resp, nil
}
