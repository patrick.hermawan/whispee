package service

import (
	"gochassis/common"
	"gochassis/ent"
)

type Service struct {
	Env  common.Env
	Repo *ent.Client
}
