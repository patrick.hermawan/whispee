package server

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/gin-gonic/gin"
)

type Options struct {
	Port        int
	ReadTimeout int
	Debug       bool
	Middlewares []string // Logger, Cors
}

type AppServer struct {
	Engine  *gin.Engine
	options Options
}

type ContextFunc gin.HandlerFunc

func New(options Options) *AppServer {
	return &AppServer{
		Engine:  gin.Default(),
		options: options,
	}
}

func (s *AppServer) Start() {
	srv := &http.Server{
		Addr:        fmt.Sprintf(":%v", s.options.Port),
		Handler:     s.Engine,
		ReadTimeout: time.Duration(s.options.ReadTimeout) * time.Second,
	}

	go func() {
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("error on listening to port %v, error %v", s.options.Port, err)
		}
	}()

	doGracefulShutdown(srv)
}

// doGracefulShutdown wait for 5 seconds before shutting down server
func doGracefulShutdown(srv *http.Server) {
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit
	log.Printf("shutting down server...")

	// The context is used to inform the server it has 5 seconds to finish
	// the request it is currently handling
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second) // TODO: use env variable
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("server forced to shutdown: ", err)
	}
	log.Println("server exiting")
}
