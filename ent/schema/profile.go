package schema

import (
	"time"

	"entgo.io/ent"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
)

type Profile struct {
	ent.Schema
}

func (Profile) Fields() []ent.Field {
	return []ent.Field{
		field.String("phone_number").Optional(),
		field.String("device_id").NotEmpty(),
		field.String("displayname").NotEmpty(),
		field.String("password").Optional().Sensitive(),
		field.Int16("birth_year").Optional(),
		field.String("gender").Optional(),
		field.Time("created_at").Default(time.Now),
		field.Time("updated_at").Default(time.Now).UpdateDefault(time.Now),
		field.Time("deleted_at").Optional(), // TODO implement soft-delete
	}
}

func (Profile) Edges() []ent.Edge {
	return []ent.Edge{
		edge.To("chats", Chat.Type),
		edge.To("chat_rooms", ChatRoom.Type),
		edge.To("posts", Post.Type),
		edge.To("likes", Post.Type),
	}
}
