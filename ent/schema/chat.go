package schema

import (
	"time"

	"entgo.io/ent"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
)

type Chat struct {
	ent.Schema
}

func (Chat) Fields() []ent.Field {
	return []ent.Field{
		field.String("text").NotEmpty(),
		field.String("attachment").Optional(),      // TODO implement attachments
		field.String("attachment_type").Optional(), // TODO implement attachments
		field.String("frontend_uuid").Unique(),     // prevent duplicates
		field.Time("created_at").Default(time.Now),
		field.Time("updated_at").Default(time.Now).UpdateDefault(time.Now),
		field.Time("deleted_at").Optional(), // TODO implement soft-delete
		field.Int("reply_to").Optional(),
	}
}
func (Chat) Edges() []ent.Edge {
	return []ent.Edge{
		edge.From("profile", Profile.Type).
			Ref("chats").
			Unique().
			Required(),
		edge.From("chat_room", ChatRoom.Type).
			Ref("chats").
			Unique().
			Required(),
		edge.To("replies", Chat.Type).
			From("reply_to_edge").
			Field("reply_to").
			Unique(),
	}
}
