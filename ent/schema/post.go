package schema

import (
	"time"

	"entgo.io/ent"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
)

type Post struct {
	ent.Schema
}

func (Post) Fields() []ent.Field {
	return []ent.Field{
		field.String("image").NotEmpty(), // TODO is it link? UUID?
		field.Text("content").NotEmpty(),
		field.String("profile_displayname").NotEmpty(), // copied from profile
		field.Int16("profile_birth_year").Optional(),   // copied from profile
		field.String("profile_gender").Optional(),      // copied from profile
		field.Other("coordinates", &Point{}).SchemaType(Point{}.SchemaType()),
		field.Time("created_at").Default(time.Now),
		field.Time("updated_at").Default(time.Now).UpdateDefault(time.Now),
		field.Time("deleted_at").Optional(), // TODO implement soft-delete
		field.Int("reply_to").Optional(),
	}
}

func (Post) Edges() []ent.Edge {
	return []ent.Edge{
		edge.To("chat_rooms", ChatRoom.Type),
		edge.From("posted_by", Profile.Type).
			Ref("posts").
			Unique().
			Required(),
		edge.From("liked_by", Profile.Type).
			Ref("likes"),
		edge.To("replies", Post.Type).
			From("reply_to_edge").
			Field("reply_to").
			Unique(),
	}
}
