package schema

import (
	"time"

	"entgo.io/ent"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
)

type ChatRoom struct {
	ent.Schema
}

func (ChatRoom) Fields() []ent.Field {
	return []ent.Field{
		field.String("room_name").NotEmpty(), // TODO UUID?
		field.Time("created_at").Default(time.Now),
		field.Time("updated_at").Default(time.Now).UpdateDefault(time.Now),
		field.Time("deleted_at").Optional(), // TODO implement soft-delete
	}
}

func (ChatRoom) Edges() []ent.Edge {
	return []ent.Edge{
		edge.From("post", Post.Type).
			Ref("chat_rooms").
			Unique(),
		edge.To("chats", Chat.Type),
		edge.From("profiles", Profile.Type).
			Ref("chat_rooms").
			Required(),
	}
}
