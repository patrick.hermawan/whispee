// Code generated by ent, DO NOT EDIT.

package ent

import (
	"fmt"
	"gochassis/ent/post"
	"gochassis/ent/profile"
	"gochassis/ent/schema"
	"strings"
	"time"

	"entgo.io/ent"
	"entgo.io/ent/dialect/sql"
)

// Post is the model entity for the Post schema.
type Post struct {
	config `json:"-"`
	// ID of the ent.
	ID int `json:"id,omitempty"`
	// Image holds the value of the "image" field.
	Image string `json:"image,omitempty"`
	// Content holds the value of the "content" field.
	Content string `json:"content,omitempty"`
	// ProfileDisplayname holds the value of the "profile_displayname" field.
	ProfileDisplayname string `json:"profile_displayname,omitempty"`
	// ProfileBirthYear holds the value of the "profile_birth_year" field.
	ProfileBirthYear int16 `json:"profile_birth_year,omitempty"`
	// ProfileGender holds the value of the "profile_gender" field.
	ProfileGender string `json:"profile_gender,omitempty"`
	// Coordinates holds the value of the "coordinates" field.
	Coordinates *schema.Point `json:"coordinates,omitempty"`
	// CreatedAt holds the value of the "created_at" field.
	CreatedAt time.Time `json:"created_at,omitempty"`
	// UpdatedAt holds the value of the "updated_at" field.
	UpdatedAt time.Time `json:"updated_at,omitempty"`
	// DeletedAt holds the value of the "deleted_at" field.
	DeletedAt time.Time `json:"deleted_at,omitempty"`
	// ReplyTo holds the value of the "reply_to" field.
	ReplyTo int `json:"reply_to,omitempty"`
	// Edges holds the relations/edges for other nodes in the graph.
	// The values are being populated by the PostQuery when eager-loading is set.
	Edges         PostEdges `json:"edges"`
	profile_posts *int
	selectValues  sql.SelectValues
}

// PostEdges holds the relations/edges for other nodes in the graph.
type PostEdges struct {
	// ChatRooms holds the value of the chat_rooms edge.
	ChatRooms []*ChatRoom `json:"chat_rooms,omitempty"`
	// PostedBy holds the value of the posted_by edge.
	PostedBy *Profile `json:"posted_by,omitempty"`
	// LikedBy holds the value of the liked_by edge.
	LikedBy []*Profile `json:"liked_by,omitempty"`
	// ReplyToEdge holds the value of the reply_to_edge edge.
	ReplyToEdge *Post `json:"reply_to_edge,omitempty"`
	// Replies holds the value of the replies edge.
	Replies []*Post `json:"replies,omitempty"`
	// loadedTypes holds the information for reporting if a
	// type was loaded (or requested) in eager-loading or not.
	loadedTypes [5]bool
}

// ChatRoomsOrErr returns the ChatRooms value or an error if the edge
// was not loaded in eager-loading.
func (e PostEdges) ChatRoomsOrErr() ([]*ChatRoom, error) {
	if e.loadedTypes[0] {
		return e.ChatRooms, nil
	}
	return nil, &NotLoadedError{edge: "chat_rooms"}
}

// PostedByOrErr returns the PostedBy value or an error if the edge
// was not loaded in eager-loading, or loaded but was not found.
func (e PostEdges) PostedByOrErr() (*Profile, error) {
	if e.loadedTypes[1] {
		if e.PostedBy == nil {
			// Edge was loaded but was not found.
			return nil, &NotFoundError{label: profile.Label}
		}
		return e.PostedBy, nil
	}
	return nil, &NotLoadedError{edge: "posted_by"}
}

// LikedByOrErr returns the LikedBy value or an error if the edge
// was not loaded in eager-loading.
func (e PostEdges) LikedByOrErr() ([]*Profile, error) {
	if e.loadedTypes[2] {
		return e.LikedBy, nil
	}
	return nil, &NotLoadedError{edge: "liked_by"}
}

// ReplyToEdgeOrErr returns the ReplyToEdge value or an error if the edge
// was not loaded in eager-loading, or loaded but was not found.
func (e PostEdges) ReplyToEdgeOrErr() (*Post, error) {
	if e.loadedTypes[3] {
		if e.ReplyToEdge == nil {
			// Edge was loaded but was not found.
			return nil, &NotFoundError{label: post.Label}
		}
		return e.ReplyToEdge, nil
	}
	return nil, &NotLoadedError{edge: "reply_to_edge"}
}

// RepliesOrErr returns the Replies value or an error if the edge
// was not loaded in eager-loading.
func (e PostEdges) RepliesOrErr() ([]*Post, error) {
	if e.loadedTypes[4] {
		return e.Replies, nil
	}
	return nil, &NotLoadedError{edge: "replies"}
}

// scanValues returns the types for scanning values from sql.Rows.
func (*Post) scanValues(columns []string) ([]any, error) {
	values := make([]any, len(columns))
	for i := range columns {
		switch columns[i] {
		case post.FieldCoordinates:
			values[i] = new(schema.Point)
		case post.FieldID, post.FieldProfileBirthYear, post.FieldReplyTo:
			values[i] = new(sql.NullInt64)
		case post.FieldImage, post.FieldContent, post.FieldProfileDisplayname, post.FieldProfileGender:
			values[i] = new(sql.NullString)
		case post.FieldCreatedAt, post.FieldUpdatedAt, post.FieldDeletedAt:
			values[i] = new(sql.NullTime)
		case post.ForeignKeys[0]: // profile_posts
			values[i] = new(sql.NullInt64)
		default:
			values[i] = new(sql.UnknownType)
		}
	}
	return values, nil
}

// assignValues assigns the values that were returned from sql.Rows (after scanning)
// to the Post fields.
func (po *Post) assignValues(columns []string, values []any) error {
	if m, n := len(values), len(columns); m < n {
		return fmt.Errorf("mismatch number of scan values: %d != %d", m, n)
	}
	for i := range columns {
		switch columns[i] {
		case post.FieldID:
			value, ok := values[i].(*sql.NullInt64)
			if !ok {
				return fmt.Errorf("unexpected type %T for field id", value)
			}
			po.ID = int(value.Int64)
		case post.FieldImage:
			if value, ok := values[i].(*sql.NullString); !ok {
				return fmt.Errorf("unexpected type %T for field image", values[i])
			} else if value.Valid {
				po.Image = value.String
			}
		case post.FieldContent:
			if value, ok := values[i].(*sql.NullString); !ok {
				return fmt.Errorf("unexpected type %T for field content", values[i])
			} else if value.Valid {
				po.Content = value.String
			}
		case post.FieldProfileDisplayname:
			if value, ok := values[i].(*sql.NullString); !ok {
				return fmt.Errorf("unexpected type %T for field profile_displayname", values[i])
			} else if value.Valid {
				po.ProfileDisplayname = value.String
			}
		case post.FieldProfileBirthYear:
			if value, ok := values[i].(*sql.NullInt64); !ok {
				return fmt.Errorf("unexpected type %T for field profile_birth_year", values[i])
			} else if value.Valid {
				po.ProfileBirthYear = int16(value.Int64)
			}
		case post.FieldProfileGender:
			if value, ok := values[i].(*sql.NullString); !ok {
				return fmt.Errorf("unexpected type %T for field profile_gender", values[i])
			} else if value.Valid {
				po.ProfileGender = value.String
			}
		case post.FieldCoordinates:
			if value, ok := values[i].(*schema.Point); !ok {
				return fmt.Errorf("unexpected type %T for field coordinates", values[i])
			} else if value != nil {
				po.Coordinates = value
			}
		case post.FieldCreatedAt:
			if value, ok := values[i].(*sql.NullTime); !ok {
				return fmt.Errorf("unexpected type %T for field created_at", values[i])
			} else if value.Valid {
				po.CreatedAt = value.Time
			}
		case post.FieldUpdatedAt:
			if value, ok := values[i].(*sql.NullTime); !ok {
				return fmt.Errorf("unexpected type %T for field updated_at", values[i])
			} else if value.Valid {
				po.UpdatedAt = value.Time
			}
		case post.FieldDeletedAt:
			if value, ok := values[i].(*sql.NullTime); !ok {
				return fmt.Errorf("unexpected type %T for field deleted_at", values[i])
			} else if value.Valid {
				po.DeletedAt = value.Time
			}
		case post.FieldReplyTo:
			if value, ok := values[i].(*sql.NullInt64); !ok {
				return fmt.Errorf("unexpected type %T for field reply_to", values[i])
			} else if value.Valid {
				po.ReplyTo = int(value.Int64)
			}
		case post.ForeignKeys[0]:
			if value, ok := values[i].(*sql.NullInt64); !ok {
				return fmt.Errorf("unexpected type %T for edge-field profile_posts", value)
			} else if value.Valid {
				po.profile_posts = new(int)
				*po.profile_posts = int(value.Int64)
			}
		default:
			po.selectValues.Set(columns[i], values[i])
		}
	}
	return nil
}

// Value returns the ent.Value that was dynamically selected and assigned to the Post.
// This includes values selected through modifiers, order, etc.
func (po *Post) Value(name string) (ent.Value, error) {
	return po.selectValues.Get(name)
}

// QueryChatRooms queries the "chat_rooms" edge of the Post entity.
func (po *Post) QueryChatRooms() *ChatRoomQuery {
	return NewPostClient(po.config).QueryChatRooms(po)
}

// QueryPostedBy queries the "posted_by" edge of the Post entity.
func (po *Post) QueryPostedBy() *ProfileQuery {
	return NewPostClient(po.config).QueryPostedBy(po)
}

// QueryLikedBy queries the "liked_by" edge of the Post entity.
func (po *Post) QueryLikedBy() *ProfileQuery {
	return NewPostClient(po.config).QueryLikedBy(po)
}

// QueryReplyToEdge queries the "reply_to_edge" edge of the Post entity.
func (po *Post) QueryReplyToEdge() *PostQuery {
	return NewPostClient(po.config).QueryReplyToEdge(po)
}

// QueryReplies queries the "replies" edge of the Post entity.
func (po *Post) QueryReplies() *PostQuery {
	return NewPostClient(po.config).QueryReplies(po)
}

// Update returns a builder for updating this Post.
// Note that you need to call Post.Unwrap() before calling this method if this Post
// was returned from a transaction, and the transaction was committed or rolled back.
func (po *Post) Update() *PostUpdateOne {
	return NewPostClient(po.config).UpdateOne(po)
}

// Unwrap unwraps the Post entity that was returned from a transaction after it was closed,
// so that all future queries will be executed through the driver which created the transaction.
func (po *Post) Unwrap() *Post {
	_tx, ok := po.config.driver.(*txDriver)
	if !ok {
		panic("ent: Post is not a transactional entity")
	}
	po.config.driver = _tx.drv
	return po
}

// String implements the fmt.Stringer.
func (po *Post) String() string {
	var builder strings.Builder
	builder.WriteString("Post(")
	builder.WriteString(fmt.Sprintf("id=%v, ", po.ID))
	builder.WriteString("image=")
	builder.WriteString(po.Image)
	builder.WriteString(", ")
	builder.WriteString("content=")
	builder.WriteString(po.Content)
	builder.WriteString(", ")
	builder.WriteString("profile_displayname=")
	builder.WriteString(po.ProfileDisplayname)
	builder.WriteString(", ")
	builder.WriteString("profile_birth_year=")
	builder.WriteString(fmt.Sprintf("%v", po.ProfileBirthYear))
	builder.WriteString(", ")
	builder.WriteString("profile_gender=")
	builder.WriteString(po.ProfileGender)
	builder.WriteString(", ")
	builder.WriteString("coordinates=")
	builder.WriteString(fmt.Sprintf("%v", po.Coordinates))
	builder.WriteString(", ")
	builder.WriteString("created_at=")
	builder.WriteString(po.CreatedAt.Format(time.ANSIC))
	builder.WriteString(", ")
	builder.WriteString("updated_at=")
	builder.WriteString(po.UpdatedAt.Format(time.ANSIC))
	builder.WriteString(", ")
	builder.WriteString("deleted_at=")
	builder.WriteString(po.DeletedAt.Format(time.ANSIC))
	builder.WriteString(", ")
	builder.WriteString("reply_to=")
	builder.WriteString(fmt.Sprintf("%v", po.ReplyTo))
	builder.WriteByte(')')
	return builder.String()
}

// Posts is a parsable slice of Post.
type Posts []*Post
