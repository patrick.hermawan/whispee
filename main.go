package main

import (
	"context"
	"fmt"
	"gochassis/common"
	"gochassis/controller"
	"gochassis/ent"
	"gochassis/infra/logger"
	"gochassis/server"
	"gochassis/service"
	"log"
	"net/http"

	_ "github.com/go-sql-driver/mysql"
	socketio "github.com/googollee/go-socket.io"
)

func main() {
	env := common.GetEnv()
	dbConn, err := ent.Open(
		"mysql",
		fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?parseTime=True", env.DbUser, env.DbPassword, env.DbHost, env.DbPort, env.DbName),
	)
	if err != nil {
		log.Fatalf("failed opening connection to mysql: %v", err)
	}
	defer dbConn.Close()
	if err = dbConn.Schema.Create(context.Background()); err != nil {
		log.Fatalf("failed creating schema resources: %v", err)
	}

	service := service.Service{Env: env, Repo: dbConn}
	controller := controller.Controller{
		Service:      service,
		Logger:       *logger.New(env.LogLevel),
		SocketServer: socketio.NewServer(nil),
	}

	controller.SocketServer.OnConnect("/chat", controller.SocketConnect)
	controller.SocketServer.OnEvent("/chat", "create", controller.SocketChatRoomCreate)
	controller.SocketServer.OnEvent("/chat", "join", controller.SocketChatRoomJoin)
	controller.SocketServer.OnEvent("/chat", "check", controller.SocketChatRoomsGet) // check which chat rooms I'm subscribed to
	controller.SocketServer.OnEvent("/chat", "get", controller.SocketChatGet)        // get chats (what have I missed)
	controller.SocketServer.OnEvent("/chat", "send", controller.SocketChatSend)
	controller.SocketServer.OnError("/chat", controller.SocketError)
	controller.SocketServer.OnDisconnect("/chat", controller.SocketDisconnect)

	go controller.SocketServer.Serve()
	defer controller.SocketServer.Close()
	http.Handle("/socket.io/", controller.SocketServer)

	authProfile, err := controller.GetProfileMiddleware()

	log.Printf("OPENING PORT: %s", env.Port)
	server := server.New(server.Options{
		Port:        env.Port,
		ReadTimeout: env.HTTPRequestTimeout,
	})

	publicGroup := server.Engine.Group("/public")
	publicGroup.GET("/ping", controller.PingHandler)
	publicGroup.POST("/profile", controller.ProfileCreate)
	publicGroup.POST("/login", authProfile.LoginHandler)
	publicGroup.GET("/post/:id", controller.PostGet)
	publicGroup.GET("/post", controller.PostsGet)

	profileGroup := server.Engine.Group("/user")
	profileGroup.Use(authProfile.MiddlewareFunc())
	profileGroup.PATCH("/profile", controller.ProfileUpdate)
	profileGroup.POST("/post", controller.PostCreate)
	profileGroup.GET("/post", controller.PostsGetJWT)
	profileGroup.POST("/chat/start", controller.ChatRoomCreate)

	go server.Start()
}
